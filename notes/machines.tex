\documentclass{article}
\usepackage[margin=1in]{geometry}
\usepackage[parfill]{parskip} % no indent, line between paragraphs
\usepackage{xcolor}
\usepackage{xspace}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{alltt}
\usepackage{mathpartir}

\newcommand\mv[1]{\textit{#1}}

\newcommand\cekto{~\longmapsto_\mathrm{cek}~}
\newcommand\erto{\longmapsto_\mathrm{er}}
\newcommand\clo[1]{\left<#1\right>}
\newcommand\env{\mathcal{E}}
\newcommand\kont{\kappa}
\newcommand\kc[2]{(\mathsf{#1}~#2)}
\newcommand\cnf[1]{\left<~#1~\right>}
\newcommand\CV{\mathit{CV}}
\newcommand\CW{\mathit{CW}}
\newcommand\CM{\mathit{CM}}
\newcommand\CN{\mathit{CN}}
\newcommand\Op{\mathit{Op}}
\newcommand\extend[3]{#1[#2 \mapsto #3]}
\newcommand\lam[2]{(\lambda #1 #2)}

\definecolor{LightBlue}{RGB}{240,240,255}
\definecolor{LightRose}{RGB}{255,240,240}
\newcommand\hl[1]{\colorbox{yellow}{\(\mathstrut#1\)}}
\newcommand\St[1]{\colorbox{LightBlue}{\(\mathstrut#1\)}}
\newcommand\Dyn[1]{\colorbox{LightRose}{\(\mathstrut#1\)}}

\newcommand\VV{\bar{V}}
\newcommand\WW{\bar{W}}

\newcommand\ecnf[1]{\mathsf{eval}\!\left<~#1~\right>}
\newcommand\rcnf[1]{\mathsf{ret}\!\left<~#1~\right>}
\renewcommand\erto{~\longmapsto_\mathrm{er}~}
\newcommand\closure[1]{(\mathsf{closure}~ #1)}

\newcommand\errto{~\longmapsto_\mathrm{er2}~}

\newcommand\bin[3]{\ensuremath{\texttt{(#1~}#2\texttt{~}#3\texttt{)}}}
\newcommand\alt{~~\vert~~}
\newcommand\qq[1]{[\![#1]\!]}

\title{Abstract Machines}

\begin{document}

\section*{Abstract Machines}

%% ============================================================
\subsection*{CEK Machine}

\[
\begin{array}{rcl}
  \CM, \CN &\texttt{~::=~}& \clo{M, \env}
  \\
  \CV, \CW &\texttt{~::=~}& \clo{V, \env}
  \\
  \env &\texttt{~::=~}& \emptyset \alt \extend{\env}{X}{\CV}
  \\
  \kont &{\texttt{~::=~}}& \mathsf{halt}
    \alt \kc{apl}{\CN, \kont}
    \alt \kc{apr}{\CV, \kont}
    \alt \kc{op}{\Op, (\CN\dots), (\CV\dots), \kont}
  \\[2pt] ~ &\alt&
    \kc{op1}{\Op^1, \kont}
    \alt \kc{op2l}{\Op^2, \CN, \kont}
    \alt \kc{op2r}{\Op^2, \CV, \kont}
\end{array}
\]
\begin{align*}
  \cnf{ \clo{(M N), \env}, \kont }
  &\cekto \cnf{ \clo{M, \env}, \kc{apl}{\clo{N, \env}, \kont)} }
  \\
  \cnf{ \CV, \kc{apl}{\CN, \kont} }
  &\cekto \cnf{ \CN, \kc{apr}{\CV, \kont} }
  \\
  \cnf{ \CV, \kc{apr}{\lam{X}{M}, \env}, \kont }
  &\cekto \cnf{ \clo{M, \extend{\env}{X}{CV}}, \kont }
  \\
  \cnf{ \clo{(\Op~ M N ...), \env}, \kont }
  &\cekto \cnf{ \clo{M, \env}, \kc{op}{\Op, (), (\clo{N, \env} \dots)} }
  \\
  \cnf{ \CV, \kc{op}{\Op, (\CW\dots), (\CM, \CN\dots), \kont} }
  &\cekto \cnf{ \CN, \kc{op}{\Op, (\CW\dots, \CV), (\CN\dots), \kont} }
  \\
  \cnf{ \clo{B_k, \env_k}, \kc{op}{\Op, (\clo{B_1, \env_1}\dots), (), \kont} }
  &\cekto \cnf{ \clo{V, \emptyset}, \kont } \qquad\text{where $V = \delta(\Op, B_1, \dots, B_k)$}
  \\
  \cnf{ \clo{X, \env}, \kont }
  &\cekto \cnf{ \CV, \kont } \qquad\text{where $CV = \mathrm{lookup}(\env, X)$}
\end{align*}

%% ============================================================
\subsection*{Eval-Return Machine}

\newcommand\evalmode{\textsf{eval}\xspace}
\newcommand\retmode{\textsf{ret}\xspace}

%% The ER machine has two kinds (or ``modes'') of configurations:
%% \evalmode mode for evaluating an expression and \retmode mode for
%% returning a value to the continuation.
%% %
%% In \textsf{eval} mode, the expression (program counter) and
%% environment are kept as separate registers. They are only combined
%% into a closure to represent the \emph{value} of a
%% $\lambda$-abstraction.

\[
\begin{array}{rrl}
  \VV, \WW &{\texttt{~::=~}}& B \alt \closure{\St{X, M,} \Dyn{\env}}
  \\[2pt]
  \kont &{\texttt{~::=~}}& \St{\mathsf{halt}}
    \alt (\St{\mathsf{apl}~ N,} \Dyn{\env, \kont})
    \alt (\St{\mathsf{apr}~} \Dyn{\VV, \kont})
    \alt (\St{\mathsf{op}~ \Op, (N\dots),} \Dyn{(\VV\dots), \env, \kont})
  \\[2pt] ~ &\alt&
    (\St{\mathsf{op1}~ \Op^1,} \Dyn{\kont})
    \alt (\St{\mathsf{op2l}~ \Op^2, N,} \Dyn{\env, \kont})
    \alt (\St{\mathsf{op2r}~ \Op^2,} \Dyn{\VV, \kont})
  \\[2pt]
  \mathit{Config} &{\texttt{~::=~}}&
    \St{\mathsf{eval}\!\left<~ M,\!\!\right.}\Dyn{\env, \kont}\St{\left.\!\!\right>}
    \alt
    \St{\mathsf{ret}\!\left<~\!\!\right.} \Dyn{\VV, \kont}\St{\left.\!\!\right>}
\end{array}
\]

\begin{align*}
  \intertext{Rules for values:}
  \ecnf{ B, \env, \kont }
  &\erto \rcnf{ B, \kont }
  \\
  \ecnf{ \lam{X}{M}, \env, \kont }
  &\erto \rcnf{ \closure{X, M, \env}, \kont }
  \\
  \ecnf{ X, \env, \kont }
  &\erto \rcnf{ \VV, \kont } \qquad\text{where $\VV = \mathrm{lookup}(\env, X)$}
  \\
  \intertext{Rules for function application:}
  \ecnf{ (M N), \env, \kont }
  &\erto \ecnf{ M, \env, \kc{apl}{N, \env, \kont} }
  \\
  \rcnf{ \VV, \kc{apl}{N, \env, \kont} }
  &\erto \ecnf{ N, \env, \kc{apr}{\VV, \kont} }
  \\
  \rcnf{ \VV, \kc{apr}{\closure{X, M, \env}, \kont} }
  &\erto \ecnf{ M, \extend{\env}{X}{\VV}, \kont }
  \\
  \intertext{Generic rules for $\Op$:}
  \ecnf{ (\Op~ M N\dots), \env, \kont }
  &\erto \ecnf{ M, \env, \kc{op}{\Op, (N\dots), (), \env, \kont} }
  \\
  \rcnf{ \VV, \kc{op}{\Op, (M N\dots), (\WW\dots), \env, \kont} }
  &\erto \ecnf{ N, \env, \kc{op}{\Op, (N\dots), (\WW\dots, \VV), \env, \kont} }
  \\
  \rcnf{ \VV_k, \kc{op}{\Op, (), (\VV_1\dots), \env, \kont} }
  &\erto \rcnf{ \WW, \kont } \qquad\text{where $\WW = \delta(\Op, \VV_1, \dots, \VV_k)$}
  \\
  \intertext{Specialized rules for $\Op^1$:}
  \ecnf{ (\Op^1~ M), \env, \kont }
  &\erto \ecnf{ M, \env, \kc{op1}{\Op^1, \kont} }
  \\
  \rcnf{ \VV, \kc{op1}{\Op^1, \kont} }
  &\erto \rcnf{ \WW, \kont } \qquad\text{where $\WW = \delta(\Op^1, \VV)$}
  \\
  \intertext{Specialized rules for $\Op^2$:}
  \ecnf{ (\Op^2~ M N), \env, \kont }
  &\erto \ecnf{ M, \env, \kc{op2l}{\Op^2, N, \env, \kont} }
  \\
  \rcnf{ \VV, \kc{op2l}{\Op^2, N, \env, \kont} }
  &\erto \ecnf{ N, \env, \kc{op2r}{\Op^2, \VV, \kont} }
  \\
  \rcnf{ \VV_2, \kc{op2r}{\Op^2, \VV_1, \kont} }
  &\erto \rcnf{ \WW, \kont } \qquad\text{where $\WW = \delta(\Op^2, \VV_1, \VV_2)$}
\end{align*}

\section*{Notes on the Eval-Return Machine}

The CEK machine is close to an implementation, but there are still a
few issues.
\begin{enumerate}
\item $\CV$ includes $\cnf{B, \env}$, even though the environment
  is completely useless, since $B$ cannot contain variables.

\item The notation for closed expressions, $\cnf{ M, \env }$ suggests
  that we repeatedly allocate data structures pairing the control expression and
  environment at each step.

   We would prefer to keep them as distinct registers. We only need to allocate
   a \emph{closure} when we reach a value $V$ (and only when that value is a
   $\lambda$ abstraction; see point \#1).
\end{enumerate}

All of the machines since the SCC machine have operated in two modes:
\begin{itemize}
\item \textbf{eval}: behavior determined by the \emph{control} expression to
  evaluate
\item \textbf{return}: we have a value, and behavior determined by the
  \emph{context} (or \emph{continuation})
\end{itemize}

In the implementation we want, the switch from eval-mode to
return-mode is when
\begin{itemize}
\item the environment is no longer needed (if the value is a $B$), or
\item a closure must be created to pair the environment with a $\lambda$ abstraction
\end{itemize}

Let's modify the CEK machine to make these two modes explicit.
I'll call this the ``Eval-Return'' machine. It is not in the book, but I think
of it as an obvious refinement of the CEK machine.

In ``eval'' mode, we can have separate \emph{control} and \emph{environment}
registers.  In ``return'' mode, there is no environment; it is either unneeded
(for $B$ values) or part of the value closure (for $\lambda$ abstractions).
The two modes are represented by two kinds of machine configurations:
\[
  \mv{ER-Config} ::= \ecnf{ M, \env, \kont} \alt \rcnf{ V, \kont }
\]

We simplify closed values. See the grammar for $\VV$.

We simplify continuations to keep the environment separate:
\begin{align*}
  \kont &::= \mathsf{halt}
  \tag*{represents $[]$}
  \\
  &\alt \kc{apl}{N, \env, \kont}
  \tag*{represents $E[([]~N')]$, where $\kont$ rep $E$ and $\clo{N, \env}$ rep $N'$}
  \\
  &\alt \kc{apr}{\VV, \kont}
  \tag*{represents $E[(V [])]$, where $\kont$ rep $E$ and $\VV$ rep $V$}
  \\
  &\alt \kc{op}{\Op, (N\dots), (\VV\dots), \env, \kont}
  \tag*{represents $E[(\Op~\VV\dots~[]~N\dots)]$, where etc.}
\end{align*}

As with the CEK machine, the ER machine never creates a new expression during
evaluation. Consequently, many parts of the configurations are constrained to a
finite number of combinations determined by the original program. The grammars
for the ER machine highlight these constrained parts in blue; the parts of the
configurations that are ``unpredictable'' are highlighted in red.

\end{document}
